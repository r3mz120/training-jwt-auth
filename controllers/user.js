const validator = require("validator");
const bcrypt = require("bcryptjs");
const helpers = require("../helpers");
const User = require("../models").User;

const controller = {
  register: async (req, res) => {
    const { email, password, nume, prenume } = req.body;

    if (!email) {
      res.status(400).send({ message: "Email not provided!" });
    } else if (!validator.isEmail(email)) {
      res.status(400).send({ message: "Email is invalid!" });
    } else if (!password) {
      res.status(400).send({ message: "Password not provided!" });
    } else {
      try {
        let user = await User.findOne({ email });
        if (!user) {
          const encryptedPassword = await bcrypt.hash(password, 10);
          user = await User.create({
            email,
            nume,
            prenume,
            password: encryptedPassword,
          });
          const token = helpers.generateToken(user.id, email);
          res.status(201).send({ id: user.id, token });
        } else {
          res
            .status(409)
            .send({ message: "An account exista already with this email!" });
        }
      } catch (err) {
        console.log(err);
        res.status(500).send({ message: "Server error!" });
      }
    }
  },

  login: async (req, res) => {
    const { email, password } = req.body;
    if (!email) {
      res.status(400).send({ message: "Email not provided!" });
    } else if (!validator.isEmail(email)) {
      res.status(400).send({ message: "Email is invalid!" });
    } else if (!password) {
      res.status(400).send({ message: "Password not provided!" });
    } else {
      try {
        let user = await User.findOne({ where: { email } });
        if (!user || !(await bcrypt.compare(password, user.password))) {
          res.status(404).send({ message: "Invalid credentials!" });
        } else {
          const token = helpers.generateToken(user.id, user.email);
          res.status(200).send({ id: user.id, token });
        }
      } catch (err) {
        console.log(err);
        res.status(500).send({ message: "Server error!" });
      }
    }
  },
};

module.exports = controller;
