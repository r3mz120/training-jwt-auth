module.exports = (sequelize, DataTypes) => {
  return sequelize.define("user", {
    nume: DataTypes.STRING,
    prenume: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
  });
};
