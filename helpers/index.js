const jwt = require("jsonwebtoken");

const helpers = {
  generateToken: (id, email) => {
    return jwt.sign({ id, email }, process.env.JWT_KEY, { expiresIn: "2h" });
  },
};

module.exports = helpers;
