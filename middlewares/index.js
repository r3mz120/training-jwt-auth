const jwt = require("jsonwebtoken");
const User = require("../models").User;

const middlewares = {
  auth: async (req, res, next) => {
    let token;
    try {
      token = req.headers["authorization"].replace("Bearer ", "");
    } catch (err) {
      res
        .status(401)
        .send({ message: "A token is required for authentification!" });
    }
    if (token) {
      let decoded;
      try {
        decoded = jwt.verify(token, process.env.JWT_KEY);
      } catch (err) {
        res.status(401).send({ message: "Invalid token!" });
      }
      if (decoded) {
        let user = await User.findByPk(decoded.id);
        if (!user) {
          res.status(401).send({ message: "User not found!" });
        } else {
          user = { id: decoded.id, email: user.email };
          req.user = user;
          next();
        }
      }
    }
  },
};

module.exports = middlewares;
